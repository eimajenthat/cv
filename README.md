# Resume PDF Generator
by Jamie Adams

A fairly simple script to generate a nicely formatted PDF resume from YAML markup.

## Requirements
- PHP 7 or greater
- Composer
- Dependencies listed in composer.json

## Installation
1. Open terminal and `cd` to desired install directory.
2. Run ```git clone gitlab.com/eimajenthat/cv```
3. Run ```cd cv```
4. Run ```composer install```

## Usage
1. Open terminal to project root.  I use Cygwin on ConEmu in Windows 10.  Others will probably work, but adjustments might be needed.  I preview my PDFs in PDFXEditor, but any PDF reader will do.
2. Run ```php run.php && /cygdrive/c/Program\ Files/Tracker\ Software/PDF\ Editor/PDFXEdit.exe build/james-adams-resume.pdf```
3. Inspect the PDF preview and ensure the output it is to your liking.
4. Close PDF viewer.
5. Retrieve generated PDF from build/ directory.