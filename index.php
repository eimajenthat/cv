<?php
date_default_timezone_set('America/Chicago');
require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Yaml\Yaml;

$data = Yaml::parse(file_get_contents(__DIR__.'/resume.yaml'));

function titleize($str) {
  return ucwords(str_replace(['-','_'], ' ', $str));
}

include __DIR__.'/template.phtml';
