<?php
date_default_timezone_set('America/Chicago');
require_once __DIR__.'/vendor/autoload.php';

ob_start();
include __DIR__.'/index.php';
$html = ob_get_clean();

// HTML
file_put_contents(__DIR__.'/build/james-adams-resume.html', $html);

// DOMPDF
/*
// Convert to PDF
// disable DOMPDF's internal autoloader if you are using Composer
define('DOMPDF_ENABLE_AUTOLOAD', false);

// include DOMPDF's default configuration
require_once __DIR__.'/dompdf-config.php';

$dompdf = new DOMPDF();
$dompdf->load_html($html);
$dompdf->render();
$output = $dompdf->output();

file_put_contents(__DIR__.'/build/james-adams-resume.pdf', $output);
*/

// mPDF
$mpdf=new \Mpdf\Mpdf();
$mpdf->WriteHTML($html);
$mpdf->dpi = 128;
$mpdf->Output(__DIR__.'/build/james-adams-resume.pdf');
exit;
