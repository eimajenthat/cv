---
foo: bar
---
This is **strong**.

# James Adams

## Relevant Skills & Experience

### Programming

- Experience building large scale web applications in PHP, JavaScript, CSS, HTML, SQL.
- Experience programming in Python, Ruby, Node.js, Groovy, Bash, Perl, Java, FileMaker.
- Experience building rich user interfaces on the web using HTML/CSS, jQuery, MooTools, LessCSS, Twitter Bootstrap.
- Solid background working with CSS Selectors and XPath, as well as regular expressions.
- Integrate third party technologies using RESTful Web Services, JSON, XML, native SDKs, ODBC.
- Building solutions using existing libraries and frameworks, including Zend and Symfony2.
- Database programming using Doctrine, ORM and raw SQL, as well as using special purpose libraries to work with MongoDB.
- Use of Git and SVN version control to manage commits and branches.
- Use of XDebug, Firebug, and Chrome Developer Tools to debug PHP and JavaScript code.
- Understanding of Agile principles and methodologies, including scrum meetings, rapid development cycles (sprints), and test-driven development.

### System Administration

- Configuration, deployment, maintenance, troubleshooting, and repair of all types of PCs, laptops, servers and printers, including experience with RAID, partitioning tools, automated deployment tools, network printers.
- Configure and maintain server resources, including Apache, Nginx, APC, MySQL, Samba, FileMaker, Active Directory, and various network applications.
- Support, use, and configuration of common network protocols, such as HTTP, FTP, POP3, SMTP, IMAP4, MS Exchange, SSH, Remote Desktop, VNC, DNS; and network tools, such as ipconfig, tracert, ping, nslookup, telnet, whois, nmap, and wireshark/ethereal.
- Experience installing, configuring, and troubleshooting wired and wireless network equipment.

### General Purpose

- Excellent verbal and written communication skills.
- Problem solving and conflict resolution.
- Ability to acquire new skills with minimal assistance.

## Employment History



<div class="main-section">
    <span class="job-title">Freelance Software Engineer</span> <span class="job-timeframe">2013 - Present</span>
    <div class="">
        <div class="employer">
            <address>
                <strong>James Adams Technology Consulting</strong><br>
                1304 Glenwood Cove<br>
                Round Rock, TX 78681<br>
                <abbr title="Phone">P:</abbr> (512) 921-7085
            </address>
        </div>

- Built custom web applications in PHP, using Symfony2 and other MVC frameworks.
- Built web services client libraries using PHP and Curl.
- Manage commits and feature branches with SVN and Git/GitHub.
- Document, track, and resolve bugs, issues, and feature requests in client bug tracking systems (including Jira and Trac).
- Built and modified MVC models backed by Doctrine ORM.
- Create PHP/HTML templates based on Photoshop PSD comps.
- Built custom URL shortener to generate short URLS and route them various public application resources.
- Worked with numerous PHP MVC frameworks, including Yii, Symfony2, Zend, and others.
- Built custom logging solution to capture present searchable filtered list of transaction logs for application admins to use in dealing with payment processing issues.
- Built custom solution in Ruby to generate nicely formatted invoices in HTML, PDF, and TXT, based on CSV reports.  Used Liquid Templates, wkHTMLtoPDF, PDFKit, and Ruby's Money and CSV gems.
- Helped build a REST API backend for iOS/Android mobile apps, using Yii PHP.

    <span class="job-title">Software Engineer</span> <span class="job-timeframe">2011 - 2012</span>
    <div class="">
        <div class="employer">
            <img src="http://www.liveoak360.com/images/logo.jpg" class="employer-logo">
            <address>
                <strong>Live Oak 360</strong><br>
                PO Box 80001<br>
                Austin, TX 78708<br>
                <abbr title="Phone">P:</abbr> (512) 733-8800
            </address>
        </div>
- Helped to build a large scale PHP MVC application, based on the Zend framework.</li>
- Built calendar/event system for social network site, including event pages, scheduling, guest lists, invites, RSVPs, and handling unregistered users.</li>
- Troubleshoot and resolve bugs with any aspect of a large social network application, from CSS/HTML rendering, to backend PHP, to database.</li>
- Built session-based login-redirect system to correctly route users back to their desired page upon successful login, even after multiple login attempts.</li>
- Built and modified Zend models backed by Doctrine ORM.</li>
- Manage commits and feature branches with SVN and Git/GitHub.</li>
- Deployed software updates and database schema migrations using git and custom scripting.</li>
- Researched and implemented best practices for performance tuning of APC (Alternative PHP Cache).</li>
- Refactored URL scheme for entire social media application, and design compressed UUID encoding scheme to make cleaner URLs.</li>
- Integrated third-party URL shortener API to generate short URLs for Twitter sharing.</li>
- Generate complex UI styling using static CSS and LessCSS.</li>
- Develop rich user interfaces using MooTools and jQuery.</li>
- Create PHP/HTML templates based on Photoshop PSD comps.</li>
- Document, track, and resolve bugs, issues, and feature requests in FogBugz and Basecamp.</li>
- Integrate sharing for Pinterest, Twitter, and Facebook, using their provided APIs as well as ShareThis API.</li>
- Template modifications, troubleshooting, and bugfixes on customized Magento ecommerce solution.</li>

    <span class="job-title">Support Engineer, IEM and IKM</span> <span class="job-timeframe">2010 - 2011</span>
    <div class="">
        <div class="employer">
            <img src="http://www.interspire.com/images/logo.gif" class="employer-logo">
            <address>
                <strong>Interspire, Inc</strong><br>
                2711 West Anderson Lane, Suite 200<br>
                Austin, TX 78757<br>
                <abbr title="Phone">P:</abbr> (512) 758-7492
            </address>
        </div>
        <ul>

- Become an absolute expert in the products (Email Marketer and Knowledge Manager), including the installation, configuration, usage, code base, database structure, common problems, performance considerations, and related technologies.
- Review customer-submitted support tickets, reproduce described problems, troubleshoot them, and repair or suggest alternative courses of action for the customer.
- Find and document bugs and potential improvements in the product.  Build and deploy patches when feasible.
- Assist customers with Apache and MySQL server administration required to setup and maintain Interspire products, as well as setting up required cron jobs, and limited MTA configuration/troubleshooting.
- Provide more limited assistance for less common supported platforms, including Oracle, MS SQL Server, and Microsoft Internet Information Services (IIS).
- Develop minor update releases based on identified bugs, debug and write PHP and JavaScript code.
- Create development branches in SVN, commit changes, and merge back to trunk.
- Manage Agile sprints, and track bugs and improvements included in releases using Atlassian JIRA.
- Write and update knowledge base articles and other supporting documentation to empower customers to resolve issues without our assistance, and get more value out of the product.
- Interact with customers via online ticket system and customer forum, to present a professional and helpful image for the company.
- Recruit, interview, and train remote contractors all over the globe to join support team.
- Maintain open communication with a distributed support team, operating in different time zones, scheduling meetings and training.
- Compose training materials for new support personnel.
- Create reporting scripts using Python to analyze support team members' performance, and forecast future staffing needs.
- Act as an information resource for sales and customer service teams, as well as new employees.


    <span class="job-title">Database & Systems Administrator</span> <span class="job-timeframe">2005 - 2010</span>
    <div class="">
        <div class="employer">
            <img src="http://liquis.com/images/liquis-logo.png" class="employer-logo">
            <address>
                <strong>Liquis, Inc.</strong><br>
                400 Parker Dr., Ste 1110<br>
                Austin, TX 78728<br>
                <abbr title="Phone">P:</abbr> (512) 299-9634
            </address>
        </div>

- Build and maintain data entry and reporting tools for company database in FileMaker.
- Build and maintain import/export and other communication facilities between various company data resources, including FileMaker Server, MS SQL Server, MS Access, and MySQL.
- Build and maintain PHP scripts for accessing company database and e-commerce sites over the web.
- Administer company servers running Windows Server 2003, Linux, and FreeBSD.
- Designed and built a system to automatically wipe hard drives, and enter PC hardware data into company database.
- Designed and built automated shipping tool, which communicates with UPS over their XML Shipping API in order to create and print packing labels, and record the necessary order information in two separate internal database systems.
- Designed and built complex automated report generation system, eliminating several days of labor per month, previously spent manually generating reports.
- Train new employees on company software tools and procedures.


## Education & Certifications
<div class="main-section">


    <div style="overflow:hidden;float:right;width:100px;">
        <img src="http://www.galaent.com/images/ciw/ciw-logo.gif"><br>
        <img src="http://c810422.r22.cf2.rackcdn.com/wp-content/uploads/2012/10/comptia-logo.gif"><br>
        <img src="http://www.dolvin.com/images/microsoftCertified.jpg"><br>
        <img src="http://kit-consulting.net/logos/java.jpg">
    </div>

### IT Industry Certifications

- CIW Certified Internet Web Professional Site Designer
- CIW Certified Database Design Specialist
- CompTIA Network+ Certified Professional
- CompTIA Project+ Certified Professional
- CompTIA Security+ Certified Professional
- Microsoft Certified Professional (MCP): Windows XP Professional
- Sun Certified Java Associate

    <div style="overflow:hidden;float:right;width:100px;text-align:center;">
        <img src="http://www.saycampuslife.com/wp-content/uploads/2010/09/WGU_logo.png" style="width:100px;">
    </div>

### Academic Credentials

- Bachelor of Science in Information Technology <br>Western Governors University (Online), Salt Lake City, UT. 2009.
- High School Diploma<br> Huntington-Surrey School, Austin, TX. 2003.
